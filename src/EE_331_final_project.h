/*
 * Constant Definitions
 */
#define F_CPU 8000000UL // 8 MHz - internal RC oscillator, CKDIV8 fuse not programmed

#define BLANK  0x20 // Blank space
#define BLOCK  0xFF // Fully lit character block
#define DEGREE 0xDF // Degree symbol
#define SHADE1 0x00 // Shaded character block, version 1
#define SHADE2 0x01 // Shaded character block, version 2
#define COPYR  0x02 // Copyright symbol
#define BAR_L  0x03 // Left-alligned vertical bar
#define BAR_R  0x04 // Right-alligned vertical bar
#define BAR_RH 0x05 // Right-alligned vertical half-bar
#define BAR_CQ 0x06 // Center-alligned vertical quarter-bar

#define DC_FAULT_STR          0
#define DC_FAULT_CLEAR_STR    1
#define HF_OSC_STR            2
#define HF_OSC_CLEAR_STR      3
#define THERM_OVRLD_STR       4
#define THERM_OVRLD_CLEAR_STR 5
#define CHANNEL_MUTED_STR     6

#define MIN_TEMP_ADC       2    // ADC reading for minimum measurable temperature (9.7 deg.C)
#define MAX_TEMP_ADC       1022 // ADC reading for maximum measurable temperature (102.7 deg.C)
#define OVERLOAD_TEMP_ADC  773  // ADC reading for thermal overload temperature (80.0 deg.C)
#define RESET_TEMP_ADC     336  // ADC reading for thermal overload reset temperature (40.0 deg.C)
#define TEMP_UPDATE_PERIOD 30   // How often the temperature readings get updated (i.e. once out of every N LCD updates)

#define SPECIAL_DISPLAY_ON_COUNT  23
#define SPECIAL_DISPLAY_OFF_COUNT 31

#define ADC_REFERENCE 0x40

#define SS_TIMER0_COUNT 16

#define SYS_STATE_ADDRESS   1
#define TEMP_UNITS_BIT      0
#define VU_METER_SCALE_BIT  1
#define THML_OVLD_RIGHT_BIT 2
#define THML_OVLD_LEFT_BIT  3

/*
 * Functional macro definitions
 */
#define ENABLE_INTERRUPTS     sei()
#define DISABLE_INTERRUPTS    cli()
#define SET_ADC_INPUT_VU_L    ADMUX = ADC_REFERENCE | 0x00
#define SET_ADC_INPUT_VU_R    ADMUX = ADC_REFERENCE | 0x01
#define SET_ADC_INPUT_TEMP_L  ADMUX = ADC_REFERENCE | 0x02
#define SET_ADC_INPUT_TEMP_R  ADMUX = ADC_REFERENCE | 0x03
#define START_AD_CONVERSION   ADCSRA |= 0x40       
#define WAIT_FOR_ADC          while(ADCSRA & 0x40)
#define STOP_TIMER1           TCCR1B &= 0xF8
#define RESET_TIMER2          TCNT2 = 0x00
#define SET_LCD_RS            PORTC |= 0x01
#define CLEAR_LCD_RS          PORTC &= 0xFE
#define CLEAR_LCD_RW          PORTC &= 0xFD
#define SET_LCD_E             PORTC |= 0x04
#define CLEAR_LCD_E           PORTC &= 0xFB
#define SET_LCD_DB(db)        PORTB = db
#define ENABLE_SS_BYPASS      PORTC |= 0x08
#define ENABLE_LEFT_CHANNEL   PORTC |= 0x10
#define ENABLE_RIGHT_CHANNEL  PORTC |= 0x20
#define DISABLE_LEFT_CHANNEL  PORTC &= 0xEF
#define DISABLE_RIGHT_CHANNEL PORTC &= 0xDF
#define DISABLE_BOTH_CHANNELS PORTC &= 0xCF

/*
 * Test condition macro definitions
 */
#define TEMP_UNITS_SW_ACTIVE  (PIND | 0xFE) == 0xFE
#define MUTE_LEFT_SW_ACTIVE   (PIND | 0xFD) == 0xFD
#define MUTE_RIGHT_SW_ACTIVE  (PIND | 0xFB) == 0xFB
#define LEFT_DC_FAULT         (PINA | 0xEF) == 0xEF
#define RIGHT_DC_FAULT        (PINA | 0xDF) == 0xDF
#define LEFT_HF_OSCILLATIONS  (PINA | 0xBF) == 0xBF
#define RIGHT_HF_OSCILLATIONS (PINA | 0x7F) == 0x7F

/*
 * External dependencies
 */
#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

/*
 * Function signatures
 */
void initializeMCU(void);
void showStartupScreen(void);
void initializeShutdownDetection(void);
void initializeAutomaticFaultDetection(void);
void initializeNormalModeDisplay(void);
void updateLCD(void);
void updateTempDisplay(uint8_t channel);
void updateVUMeter(uint8_t channel);
void checkChannelFaultStatus(void);

void initializeLCD(void);
void LCD_putchar(char c);
void LCD_putstr(const char *str);
void LCD_putstr_P(const char *str);
void LCD_goto(uint8_t line, uint8_t pos);
void LCD_clear(void);
