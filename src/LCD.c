#include "EE_331_final_project.h"

static void LCD_addcustchars(void);
static void LCD_command(uint8_t db);

/*
 * LCD startup and initialization routine. Sets the data interface as well
 * as various display parameters. Also adds custom characters to the LCD RAM.
 */
void initializeLCD(void) {
	LCD_command(0x38);	// 8 bit data interface; 2 line display mode; 5x8 character set
	LCD_command(0x01);	// Clear display
	LCD_command(0x10);	// Shift cursor to left, address counter is decreased by 1
	LCD_command(0x06);	// Cursor moves right, shift display off
	LCD_addcustchars(); // Add custom-defined characters to character set
	LCD_command(0x0C);	// Display on, cursor off, blink off
}

static void LCD_addcustchars(void) {
	// Set character generator RAM address to 0x00 - first row of first custom
	// character (i.e. character at data display address 0x00). CGRAM address will
	// automatically increment after each write command.
	LCD_command(0x40);

	// Custom character #1. Shaded block 1.
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);

	// Custom character #2. Shaded block 2.
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);
	LCD_putchar(0x15);
	LCD_putchar(0x0A);

	// Custom character #3. Copyright symbol.
	LCD_putchar(0x0E);
	LCD_putchar(0x11);
	LCD_putchar(0x0D);
	LCD_putchar(0x11);
	LCD_putchar(0x11);
	LCD_putchar(0x0D);
	LCD_putchar(0x11);
	LCD_putchar(0x0E);

	// Custom character #4. Left-alligned vertical bar.
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);
	LCD_putchar(0x10);

	// Custom character #5. Right-alligned vertical bar.
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);

	// Custom character #6. Right-alligned vertical half-bar.
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	LCD_putchar(0x01);
	
	// Custom character #7. Center-alligned vertical quarter-bar.
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x00);
	LCD_putchar(0x04);
	LCD_putchar(0x04);
	LCD_putchar(0x04);
}

/*
 * Send a byte instruction to the LCD controller.
 */
static void LCD_command(uint8_t db) {
	SET_LCD_DB(db);
	CLEAR_LCD_RS;
	CLEAR_LCD_RW;
	SET_LCD_E;
	_delay_us(90);
	_delay_us(90);
	_delay_us(90);
	_delay_us(90);
	_delay_us(90);
	_delay_us(90);
	CLEAR_LCD_E;
}

/*
 * Print the given character on the LCD at the current DDRAM address.
 */
void LCD_putchar(char c) {
	SET_LCD_DB(c);
	SET_LCD_RS;
	CLEAR_LCD_RW;
	SET_LCD_E;
	_delay_us(38);
	CLEAR_LCD_E;
}

/*
 * Print the given character string on the LCD screen starting at the
 * current DDRAM address.
 */
void LCD_putstr(const char *str) {
	while (*str)
		LCD_putchar(*str++);
}

/*
 * Print the given character string, which is stored in program memory (i.e. a
 * string literal declared in-line with the PSTR macro or else declared with the
 * PROGMEM attribute), on the LCD screen starting at the current DDRAM address.
 */
void LCD_putstr_P(const char *str) {
	char c;
	while ((c = pgm_read_byte(str++)))
		LCD_putchar(c);
}

/*
 * Set the DDRAM address to the specified line and specified character
 * position on the LCD screen.
 *   line: 1, 2, 3, or 4
 *   pos:  1, 2, 3, ... or 20
 */
void LCD_goto(uint8_t line, uint8_t pos) {
	if (line == 1)
		LCD_command(0x80 | (pos - 1));
	else if (line == 2)
		LCD_command(0x80 | (pos + 63));
	else if (line == 3)
		LCD_command(0x80 | (pos + 19));
	else // line == 4
		LCD_command(0x80 | (pos + 83));
}

/*
 * Clear the LCD screen. Although the LCD documentation claims the
 * hardware will also reset the DDRAM address counter, this apparently
 * cannot be garaunteed. Use LCD_goto() instead.
 */
void LCD_clear(void) {
	LCD_command(0x01);
	_delay_ms(1);
}
