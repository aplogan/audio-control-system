#include "EE_331_final_project.h"


/*
 * Global constants
 */
extern const prog_uint16_t adc_to_temp_C_LUT[]; // Defined in LUT.c
extern const prog_uint16_t adc_to_temp_F_LUT[]; // Defined in LUT.c

const char vu_meter_tick_marks[] = {BAR_L,  BLANK, BAR_CQ, BLANK,  BAR_RH, BLANK,  BLANK,
                                    BAR_CQ, BLANK, BAR_R,  BLANK,  BLANK,  BAR_CQ, BLANK,
							        BAR_RH, BLANK, BLANK,  BAR_CQ, BLANK,  BAR_R, '\0'};

const uint16_t vu_adc_bar_lin_map[21] = { 25,  76, 127, 178, 229, 281, 332,
                                         383, 434, 485, 537, 588, 639, 690,
									     741, 793, 844, 895, 946, 997, 1023};
const uint16_t vu_adc_bar_log_map[21];

const char dc_fault_str[]          PROGMEM = "      DC fault!     "; 
const char dc_fault_clear_str[]    PROGMEM = "         "; 
const char hf_osc_str[]            PROGMEM = "  HF oscillations!  ";
const char hf_osc_clear_str[]      PROGMEM = "                ";
const char therm_ovrld_str[]       PROGMEM = "  Thermal overload! ";
const char therm_ovrld_clear_str[] PROGMEM = "                 ";
const char channel_muted_str[]     PROGMEM = "   Channel muted.   ";
const char* channel_status_strings[] = {dc_fault_str,    dc_fault_clear_str,
                                        hf_osc_str,      hf_osc_clear_str,
								        therm_ovrld_str, therm_ovrld_clear_str,
								        channel_muted_str};


/*
 * Global variables
 */
volatile uint8_t timer1_tick_count = 0;
volatile uint8_t old_sys_state, sys_state;
uint8_t temp_update_count = 0;
uint8_t old_vu_bars_L = 0;
uint8_t old_vu_bars_R = 0;
uint16_t old_adc_temp_L = 0xFFFF;
uint16_t old_adc_temp_R = 0xFFFF;

uint8_t left_fault_count = 0, right_fault_count = 0;

uint8_t left_DC_fault_display_counter         = 0, left_DC_fault_display_priority         = 0,
        left_HF_oscillations_display_counter  = 0, left_HF_oscillations_display_priority  = 0,
		left_thermal_overload_display_counter = 0, left_thermal_overload_display_priority = 0;

uint8_t right_DC_fault_display_counter         = 0, right_DC_fault_display_priority         = 0,
        right_HF_oscillations_display_counter  = 0, right_HF_oscillations_display_priority  = 0,
		right_thermal_overload_display_counter = 0, right_thermal_overload_display_priority = 0;


/*
 * Global flag structures
 */
struct {
	uint8_t left_DC_fault          : 1;
	uint8_t right_DC_fault         : 1;
	uint8_t left_HF_oscillations   : 1;
	uint8_t right_HF_oscillations  : 1;
	uint8_t left_thermal_overload  : 1;
	uint8_t right_thermal_overload : 1;
	uint8_t left_muted             : 1;
	uint8_t right_muted            : 1;
} channel_status_flags = {0, 0, 0, 0, 0, 0, 0, 0};

struct {
	uint8_t temp_units_sw_active : 1;
	uint8_t mute_left_sw_active  : 1;
	uint8_t mute_right_sw_active : 1;
} switch_state_flags = {0, 0, 0};

struct {
	uint8_t update_lcd            : 1;
	uint8_t timer0_schedule_mode  : 1;
	uint8_t special_display_left  : 1;
	uint8_t special_display_right : 1;
	uint8_t left_muted_displayed  : 1;
	uint8_t right_muted_displayed : 1;
} lcd_flags = {0, 0, 0, 0, 0, 0};

struct {
	uint8_t units          : 1; // 0: display Celsius temperature; 1: display Fahrenheit temperature
	uint8_t units_changed  : 2; // 2 or 1: indicates the units selection has changed since the last LCD update
	uint8_t temp_limited_L : 1; // 1: indicates that the last ADC temp measurement for left channel was past its max or min
	uint8_t temp_limited_R : 1; // 1: indicates that the last ADC temp measurement for right channel was past its max or min
} temp_meas_flags = {0, 2, 0, 0};


/*
 *
 */
int main(void) {
	initializeMCU();
	initializeLCD();
	showStartupScreen();	
	checkChannelFaultStatus();
	initializeAutomaticFaultDetection();
	initializeShutdownDetection();
	initializeNormalModeDisplay();
	
	ENABLE_INTERRUPTS;

	while (1) {
		// Update the LCD if it has been requested
		if (lcd_flags.update_lcd) {
			lcd_flags.update_lcd = 0;
			updateLCD();
		}

		// Check "change temperature units" switch
		if (TEMP_UNITS_SW_ACTIVE) {
			if (!switch_state_flags.temp_units_sw_active) {
				switch_state_flags.temp_units_sw_active = 1;
				temp_meas_flags.units ^= 1;
				temp_meas_flags.units_changed = 2;
				sys_state ^= (1 << TEMP_UNITS_BIT);
				temp_update_count = 0;
			}
		} else
			switch_state_flags.temp_units_sw_active = 0;
		
		// Check "mute left channel" switch
		if (MUTE_LEFT_SW_ACTIVE) {
			if (!switch_state_flags.mute_left_sw_active) {
				switch_state_flags.mute_left_sw_active = 1;
				channel_status_flags.left_muted ^= 1;
				if (channel_status_flags.left_muted)
					DISABLE_LEFT_CHANNEL;
				else if (!channel_status_flags.left_DC_fault &&
				         !channel_status_flags.left_HF_oscillations &&
						 !channel_status_flags.left_thermal_overload)
					ENABLE_LEFT_CHANNEL;
			}
		} else
			switch_state_flags.mute_left_sw_active = 0;
		
		// Check "mute right channel" switch
		if (MUTE_RIGHT_SW_ACTIVE) {
			if (!switch_state_flags.mute_right_sw_active) {
				switch_state_flags.mute_right_sw_active = 1;
				channel_status_flags.right_muted ^= 1;
				if (channel_status_flags.right_muted)
					DISABLE_RIGHT_CHANNEL;
				else if (!channel_status_flags.right_DC_fault &&
				         !channel_status_flags.right_HF_oscillations &&
						 !channel_status_flags.right_thermal_overload)
					ENABLE_RIGHT_CHANNEL;
			}
		} else
			switch_state_flags.mute_right_sw_active = 0;
	}

	return 0;
}


/*
 * Set power reduction register, configure I/O ports, configure/initialize peripherals.
 */
void initializeMCU(void) {
	// Set power reduction register.
	PRR = 0xB6; // 7: TWI:     off
                // 6: Timer 2: on
				// 5: Timer 0: off
				// 4: USART 1: off
				// 3: Timer 1: on
				// 2: SPI:     off
				// 1: USART 0: off
				// 0: ADC:     on
	
	// Configure I/O ports.
	PORTA = 0xF0; // PA[7:4] are digital inputs (fault detection lines). Enable pullups.
	DDRA  = 0x00; // PA[3:0] are analog inputs (VU meter & temperature measurements).
				  //         - Leave in HI-Z mode.
	
	PORTB = 0x00; // PB[7:0] are all digital outputs to the LCD. Set to low level initially.
	DDRB  = 0xFF; 

	PORTC = 0xC0; // PC[7:6] are unused outputs - set to high level to reduce power consumption.
	DDRC  = 0xFF; // PC[5:0] are digital outputs (3 relay control lines and 3 LCD control lines)
				  //         - Set at low level initially.

	PORTD = 0x0F; // PD[7:4] are unused outputs - set to high level to reduce power consumption.are inputs - enable pullups.
	DDRD  = 0xF0; // PD[3:0] are digital inputs (3 user interface switches and 1 AC power detect line).	
				  //         - Enable pullups.

	// Configure ADC
	DIDR0  = 0x0F; // Digital inputs on AD[3:0] (PA[3:0]) are disabled.
	ADCSRA = 0xC3; // Turn on ADC
				   // Start first conversion (dummy conversion which takes more cycles than the rest)
				   // Auto-triggering disabled
				   // ADC Conversion Complete Interrupt Flag cleared (by setting)
				   // Interrupt disabled
				   // Prescaler set to divide I/O frequency by 64
				   //     (resulting in clk_IO / 64 = 8MHz / 64 = 125kHz).

	// Configure Timer 1
	TCCR1A = 0x00; // OC1A and OC1B disconnected from ports; set timer to CTC mode using OCR1A as TOP value
	TIMSK1 = 0x02; // Enable compare match A interrupt
	// Further task-specific configuration of Timer 1 is done later.

	// Configure Analog Comparator
	ACSR = 0x80;  // Disable the analog comparator, disable AC interrupts.
	ACSR |= 0x10; // Clear AC interrupt flag in case it triggered on last operation.

	eeprom_busy_wait();
	sys_state = old_sys_state = eeprom_read_byte((const uint8_t *) SYS_STATE_ADDRESS);
	temp_meas_flags.units = (old_sys_state >> TEMP_UNITS_BIT) & 0x01;
	channel_status_flags.left_thermal_overload = (old_sys_state >> THML_OVLD_LEFT_BIT) & 0x01;
	channel_status_flags.right_thermal_overload = (old_sys_state >> THML_OVLD_RIGHT_BIT) & 0x01;
}


/*
 * Show a powering up sequence screen on the LCD. Include firmware version and author
 * information. Set up the soft-start bypass relay turn-on delay (~4 sec) as well as the
 * subsequent speaker relay turn-on delay (anonther ~1 sec) by setting timer 1 to clear on
 * compare match mode. Timer 1 compare match interrupt service routine will add segments
 * to a power-up progress bar as the delay sequence progresses.
 */
void showStartupScreen(void) {
	// First, ensure LCD is cleared, then display the opening screen including progress bar shadow.
	LCD_clear();
	LCD_goto(1,4);
	_delay_us(50);
	LCD_putstr_P(PSTR("Powering up..."));
	LCD_goto(2,1);
	for (uint8_t i=1; i<=10; i++) {
		LCD_putchar(SHADE1);
		LCD_putchar(SHADE2);
	}
	LCD_goto(3,3);
	LCD_putstr_P(PSTR("Firmware ver 1.0"));
	LCD_goto(4,2);
	LCD_putchar(COPYR);
	LCD_putstr_P(PSTR("2009, Aaron Logan"));
	LCD_goto(2,1);

	// Next, configure Timer 1 to provide the time delay for the startup sequence.
	// f_CPU = 8MHz, prescale = 64, TOP = 31249 --> f = 8MHz / 64 / (31249 + 1) = 4Hz
	// 20 ticks @ 4Hz = 5s
	timer1_tick_count = 0;
	TCNT1 = 0x00;  // Ensure that the timer is cleared.
	OCR1A = 31249; // Compare match every 31250 count cycles (include count cycle from 31249 back to 0)
	TCCR1B = 0x0B; // Start timer by setting clock input to clk_IO / 64
	
	// Now wait for 20 Timer 1 compare-match-clear cycles for the startup sequence to finish.
	// This simply adds one block to the progress bar with each tick and turns on the soft
	// start circuit bypass relay after a certain elapsed time. Refer to the Timer 1 compare-
	// match A interrupt service routine for details.
	ENABLE_INTERRUPTS;
	while (timer1_tick_count < 20) ;
	STOP_TIMER1;
	DISABLE_INTERRUPTS;
}


/*
 * Configure Pin Change Interrupt 0 to respond to input logic level changes on any
 * of the pins PCINT[4:7]. The corresponding interrupt service routine will decide
 * how to handle the appearance of faults or the clearing of faults indicated by
 * level changes on these inputs.
 */
void initializeAutomaticFaultDetection(void) {
	PCICR = 0x01;
	PCMSK0 = 0xF0;
}


/*
 *
 */
void initializeShutdownDetection(void) {
	// Configure Timer 2 to provide the time delay between final
	// AC power detection even and beginning of shutdown sequence. AC power
	// detection events occur at 120Hz or every 8.33ms.
	// f_CPU = 8MHz, prescale = 1024, TOP = 70 --> f = 8MHz / 1024 / (70 + 1) = 110.0Hz (T = 9.09ms)
	TCNT2 = 0x00;  // Ensure that the timer is cleared.
	TCCR2A = 0x02; // OC2A & OC2B disconnected from port
	               // Clear timer on compare match mode.
	OCR2A = 70;    // Compare match every 71 count cycles (include count cycle from 70 back to 0)
	TIMSK2 = 0x02; // Enable compare match A interrupt
	TCCR2B = 0x07; // Ignore FOC2A & FOC2B
	               // Clear timer on compare match mode
				   // Start timer by setting prescaler at divide by 1024
	
	// Enable external interrupt to respond to AC power detector
	EICRA = 0x0C; // Set external interrupt 1 to trigger on rising edge of INT1 pin
	EIMSK = 0x02; // Enable external interrupt 1
}


/*
 *
 */
void initializeNormalModeDisplay(void) {
	// First, clear the startup screen, then display the audio channel indicators.
	LCD_clear();
	LCD_goto(1,1);
	LCD_putstr_P(PSTR("Left:"));
	LCD_goto(3,1);
	LCD_putstr_P(PSTR("Right:"));
	
	// Set these flags so that the VU meter update function will display the VU meter
	// tick marks on its first time through for each channel.
	lcd_flags.special_display_left = 1;
	lcd_flags.special_display_right = 1;
	
	// Next, set these flags which will (respectively) cause the LCD to be updated
	// (temperature readings and VU meters updated) as soon as the program enters
	// its normal operation loop, and will tell the Timer 0 compare-match A interrupt
	// service routine to switch to normal mode (i.e. it was previously used for the
	// startup screen). In normal mode it sets the update_lcd flag to schedule LCD
	// updates.
	lcd_flags.update_lcd = 1;
	lcd_flags.timer0_schedule_mode = 1;
	
	// Finally, configure Timer 1 to provide the time delay between scheduled LCD updates.
	// f_CPU = 8MHz, prescale = 64, TOP = 4166 --> f = 8MHz / 64 / (4166 + 1) = 30.00Hz
	TCNT1 = 0x00;  // Ensure that the timer is cleared.
	OCR1A = 4166;  // Compare match every 4167 count cycles (include count cycle from 4166 back to 0)
	TCCR1B = 0x0B; // Start timer by setting clock input to clk_IO / 64
}


/*
 * In normal mode (not startup or shutdown mode), update the display of heatsink temperatures,
 * fault condition indicators, channel muting, and VU meter levels. Fault condition indicators
 * include "DC fault!", "HF oscillations!" (high frequency oscillations), and "Thermal overload!".
 *
 * Fault conditions, muting, and VU meters are all displayed on one line for each channel.
 * When a fault condition exists on a channel, priority is given to displaying that. Multiple
 * faults existing on the same channel will be indicated in repeating succession in the order
 * that the faults occurred. Fault inidication is displayed as flashing words (approximately 1Hz
 * flash rate) in order to draw user attention to the error condition. Next in display priority
 * is channel muting. If a channel has been muted by the user and no faults exist on that channel,
 * then "Channel muted." will be displayed for that channel. This will not be flash since the
 * user selected to have the channel muted and thus does not need to pay special attention to
 * the condition. Finally, if no faults exist on a channel and the channel is not muted, then a
 * VU meter (audio volume level) will be displayed.
 *
 * With the current settings, this function is called roughly 30 times per second, resulting in
 * an LCD refresh rate at around the limit of human persistence of vision. The upper limit of
 * the refresh rate is set by the processing speed of the MCU (i.e. a rate at which the CPU is
 * not constantly processing updates to the LCD), as well as the speed at which the LCD pixels
 * are able to toggle. This code was developed using an LCD with fairly slow pixel transition
 * speeds and thus a lower refresh rate was selected. The final limit to the refresh rate is
 * simply the point at which successive LCD states become indistinguishable to the user due to
 * persistence of vision.
 *
 * Finally, note that the temperature display does not execute every time this function is
 * called in order to save on processing time. Since the heatsink temperatures will change
 * relatively slowly due to their thermal mass, the temperature displays are refreshed only
 * once per second (with the current settings). Even this rate may be faster than absolutely
 * necessary, but it is helpful to see if the temperatures happen to be changing quickly.
 */
void updateLCD(void) {
	PORTD |= 0x80;

	// Update temperature reading displays only every TEMP_UPDATE_PERIOD updates.
	// Update temperature before updating VU meters in case a new temperature
	// overload fault is detected so that it can be displayed in lieu of the
	// VU meter for the respective channel.
	if (!temp_update_count) {
		temp_update_count = TEMP_UPDATE_PERIOD;
		updateTempDisplay(0); // Left channel
		updateTempDisplay(1); // Right channel
	} else
		temp_update_count--;
	
	if (channel_status_flags.left_DC_fault) {
		if (!left_DC_fault_display_counter) {
			left_DC_fault_display_counter = 1;
			left_DC_fault_display_priority = ++left_fault_count;
		}
		if (left_DC_fault_display_priority == 1) {
			if (left_DC_fault_display_counter == 1) {
				LCD_goto(2,1);
				LCD_putstr_P(channel_status_strings[DC_FAULT_STR]);
				lcd_flags.special_display_left = 1;
				lcd_flags.left_muted_displayed = 0;
				left_DC_fault_display_counter++;
			} else if (left_DC_fault_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (left_DC_fault_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(2,7);
					LCD_putstr_P(channel_status_strings[DC_FAULT_CLEAR_STR]);
				}
				left_DC_fault_display_counter++;
			} else {
				if (left_HF_oscillations_display_priority > left_DC_fault_display_priority)
					left_HF_oscillations_display_priority--;
				if (left_thermal_overload_display_priority > left_DC_fault_display_priority)
					left_thermal_overload_display_priority--;
				left_DC_fault_display_counter = 1;
				left_DC_fault_display_priority = left_fault_count;
			}
		}
	} else if (left_DC_fault_display_counter) {
		if (left_HF_oscillations_display_priority > left_DC_fault_display_priority)
			left_HF_oscillations_display_priority--;
		if (left_thermal_overload_display_priority > left_DC_fault_display_priority)
			left_thermal_overload_display_priority--;
		left_DC_fault_display_counter = 0;
		left_DC_fault_display_priority = 0;
		left_fault_count--;
	}

	if (channel_status_flags.left_HF_oscillations) {
		if (!left_HF_oscillations_display_counter) {
			left_HF_oscillations_display_counter = 1;
			left_HF_oscillations_display_priority = ++left_fault_count;
		}
		if (left_HF_oscillations_display_priority == 1) {
			if (left_HF_oscillations_display_counter == 1) {
				LCD_goto(2,1);
				LCD_putstr_P(channel_status_strings[HF_OSC_STR]);
				lcd_flags.special_display_left = 1;
				lcd_flags.left_muted_displayed = 0;
				left_HF_oscillations_display_counter++;
			} else if (left_HF_oscillations_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (left_HF_oscillations_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(2,3);
					LCD_putstr_P(channel_status_strings[HF_OSC_CLEAR_STR]);
				}
				left_HF_oscillations_display_counter++;
			} else {
				if (left_DC_fault_display_priority > left_HF_oscillations_display_priority)
					left_DC_fault_display_priority--;
				if (left_thermal_overload_display_priority > left_HF_oscillations_display_priority)
					left_thermal_overload_display_priority--;
				left_HF_oscillations_display_counter = 1;
				left_HF_oscillations_display_priority = left_fault_count;
			}
		}
	} else if (left_HF_oscillations_display_counter) {
		if (left_DC_fault_display_priority > left_HF_oscillations_display_priority)
			left_DC_fault_display_priority--;
		if (left_thermal_overload_display_priority > left_HF_oscillations_display_priority)
			left_thermal_overload_display_priority--;
		left_HF_oscillations_display_counter = 0;
		left_HF_oscillations_display_priority = 0;
		left_fault_count--;
	}

	if (channel_status_flags.left_thermal_overload) {
		if (!left_thermal_overload_display_counter) {
			left_thermal_overload_display_counter = 1;
			left_thermal_overload_display_priority = ++left_fault_count;
		}
		if (left_thermal_overload_display_priority == 1) {
			if (left_thermal_overload_display_counter == 1) {
				LCD_goto(2,1);
				LCD_putstr_P(channel_status_strings[THERM_OVRLD_STR]);
				lcd_flags.special_display_left = 1;
				lcd_flags.left_muted_displayed = 0;
				left_thermal_overload_display_counter++;
			} else if (left_thermal_overload_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (left_thermal_overload_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(2,3);
					LCD_putstr_P(channel_status_strings[THERM_OVRLD_CLEAR_STR]);
				}
				left_thermal_overload_display_counter++;
			} else {
				if (left_DC_fault_display_priority > left_thermal_overload_display_priority)
					left_DC_fault_display_priority--;
				if (left_HF_oscillations_display_priority > left_thermal_overload_display_priority)
					left_HF_oscillations_display_priority--;
				left_thermal_overload_display_counter = 1;
				left_thermal_overload_display_priority = left_fault_count;
			}
		}
	} else if (left_thermal_overload_display_counter) {
		if (left_DC_fault_display_priority > left_thermal_overload_display_priority)
			left_DC_fault_display_priority--;
		if (left_HF_oscillations_display_priority > left_thermal_overload_display_priority)
			left_HF_oscillations_display_priority--;
		left_thermal_overload_display_counter = 0;
		left_thermal_overload_display_priority = 0;
		left_fault_count--;
	}
	
	if (!left_fault_count) {
		if (channel_status_flags.left_muted) {
			if (!lcd_flags.left_muted_displayed) {
				LCD_goto(2,1);
				LCD_putstr_P(channel_status_strings[CHANNEL_MUTED_STR]);
				lcd_flags.left_muted_displayed = 1;
				lcd_flags.special_display_left = 1;
			}
		} else {
			lcd_flags.left_muted_displayed = 0;
			updateVUMeter(0);
		}
	}


	if (channel_status_flags.right_DC_fault) {
		if (!right_DC_fault_display_counter) {
			right_DC_fault_display_counter = 1;
			right_DC_fault_display_priority = ++right_fault_count;
		}
		if (right_DC_fault_display_priority == 1) {
			if (right_DC_fault_display_counter == 1) {
				LCD_goto(4,1);
				LCD_putstr_P(channel_status_strings[DC_FAULT_STR]);
				lcd_flags.special_display_right = 1;
				lcd_flags.right_muted_displayed = 0;
				right_DC_fault_display_counter++;
			} else if (right_DC_fault_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (right_DC_fault_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(4,7);
					LCD_putstr_P(channel_status_strings[DC_FAULT_CLEAR_STR]);
				}
				right_DC_fault_display_counter++;
			} else {
				if (right_HF_oscillations_display_priority > right_DC_fault_display_priority)
					right_HF_oscillations_display_priority--;
				if (right_thermal_overload_display_priority > right_DC_fault_display_priority)
					right_thermal_overload_display_priority--;
				right_DC_fault_display_counter = 1;
				right_DC_fault_display_priority = right_fault_count;
			}
		}
	} else if (right_DC_fault_display_counter) {
		if (right_HF_oscillations_display_priority > right_DC_fault_display_priority)
			right_HF_oscillations_display_priority--;
		if (right_thermal_overload_display_priority > right_DC_fault_display_priority)
			right_thermal_overload_display_priority--;
		right_DC_fault_display_counter = 0;
		right_DC_fault_display_priority = 0;
		right_fault_count--;
	}

	if (channel_status_flags.right_HF_oscillations) {
		if (!right_HF_oscillations_display_counter) {
			right_HF_oscillations_display_counter = 1;
			right_HF_oscillations_display_priority = ++right_fault_count;
		}
		if (right_HF_oscillations_display_priority == 1) {
			if (right_HF_oscillations_display_counter == 1) {
				LCD_goto(4,1);
				LCD_putstr_P(channel_status_strings[HF_OSC_STR]);
				lcd_flags.special_display_right = 1;
				lcd_flags.right_muted_displayed = 0;
				right_HF_oscillations_display_counter++;
			} else if (right_HF_oscillations_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (right_HF_oscillations_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(4,3);
					LCD_putstr_P(channel_status_strings[HF_OSC_CLEAR_STR]);
				}
				right_HF_oscillations_display_counter++;
			} else {
				if (right_DC_fault_display_priority > right_HF_oscillations_display_priority)
					right_DC_fault_display_priority--;
				if (right_thermal_overload_display_priority > right_HF_oscillations_display_priority)
					right_thermal_overload_display_priority--;
				right_HF_oscillations_display_counter = 1;
				right_HF_oscillations_display_priority = right_fault_count;
			}
		}
	} else if (right_HF_oscillations_display_counter) {
		if (right_DC_fault_display_priority > right_HF_oscillations_display_priority)
			right_DC_fault_display_priority--;
		if (right_thermal_overload_display_priority > right_HF_oscillations_display_priority)
			right_thermal_overload_display_priority--;
		right_HF_oscillations_display_counter = 0;
		right_HF_oscillations_display_priority = 0;
		right_fault_count--;
	}

	if (channel_status_flags.right_thermal_overload) {
		if (!right_thermal_overload_display_counter) {
			right_thermal_overload_display_counter = 1;
			right_thermal_overload_display_priority = ++right_fault_count;
		}
		if (right_thermal_overload_display_priority == 1) {
			if (right_thermal_overload_display_counter == 1) {
				LCD_goto(4,1);
				LCD_putstr_P(channel_status_strings[THERM_OVRLD_STR]);
				lcd_flags.special_display_right = 1;
				lcd_flags.right_muted_displayed = 0;
				right_thermal_overload_display_counter++;
			} else if (right_thermal_overload_display_counter < SPECIAL_DISPLAY_OFF_COUNT) {
				if (right_thermal_overload_display_counter == SPECIAL_DISPLAY_ON_COUNT) {
					LCD_goto(4,3);
					LCD_putstr_P(channel_status_strings[THERM_OVRLD_CLEAR_STR]);
				}
				right_thermal_overload_display_counter++;
			} else {
				if (right_DC_fault_display_priority > right_thermal_overload_display_priority)
					right_DC_fault_display_priority--;
				if (right_HF_oscillations_display_priority > right_thermal_overload_display_priority)
					right_HF_oscillations_display_priority--;
				right_thermal_overload_display_counter = 1;
				right_thermal_overload_display_priority = right_fault_count;
			}
		}
	} else if (right_thermal_overload_display_counter) {
		if (right_DC_fault_display_priority > right_thermal_overload_display_priority)
			right_DC_fault_display_priority--;
		if (right_HF_oscillations_display_priority > right_thermal_overload_display_priority)
			right_HF_oscillations_display_priority--;
		right_thermal_overload_display_counter = 0;
		right_thermal_overload_display_priority = 0;
		right_fault_count--;
	}
	
	if (!right_fault_count) {
		if (channel_status_flags.right_muted) {
			if (!lcd_flags.right_muted_displayed) {
				LCD_goto(4,1);
				LCD_putstr_P(channel_status_strings[CHANNEL_MUTED_STR]);
				lcd_flags.right_muted_displayed = 1;
				lcd_flags.special_display_right = 1;
			}
		} else {
			lcd_flags.right_muted_displayed = 0;
			updateVUMeter(1);
		}
	}

	PORTD &= 0x3F;
}


/*
 * INPUT: channel - either 0 or 1. 0 will measure and update heatsink temperature on the
 *                                 left channel, while 1 will measure and update heatsink
 *								   temperature on the right channel.
 */
void updateTempDisplay(uint8_t channel) {
	uint8_t lcd_line, i, adc_lower_bits, adc_upper_bits, hundreds_digit, tens_digit;
	uint16_t sum=0, adc_result, temp_display;

	// Set the proper ADC input channel to measure the temperature.
	// Also determine the line on the LCD to update for the current channel.
	if (channel) {
		SET_ADC_INPUT_TEMP_R;
		lcd_line = 3;
	} else {
		SET_ADC_INPUT_TEMP_L;
		lcd_line = 1;
	}

	PORTD |= 0x20;

	// Measure the temperature from the proper ADC input channel by
	// averaging four ADC measurements. The average should produce a
	// more accurate result by reducing noise effects.
	for (i=1; i<=4; i++) {
		START_AD_CONVERSION;
		WAIT_FOR_ADC;
		adc_lower_bits = ADCL;
		adc_upper_bits = ADCH;
		sum += (((uint16_t) adc_upper_bits) << 8) | ((uint16_t) adc_lower_bits);
	}
	adc_result = sum >> 2; // Divide the sum by four to get average.
	if (((uint8_t) sum) & 0x02) // Round up if the last two bits are 2 or 3 (the LSB is a don't care).
		adc_result++;

	PORTD &= 0xDF;

	// Compare the temperature measurement to the last one. If it is the same, then exit
	// this function provided that the temperature display units have not changed.
	// Otherwise, store the new measurement and proceed with the display routine.
	if (channel) {
		if (adc_result == old_adc_temp_R && !temp_meas_flags.units_changed)
			return;
		old_adc_temp_R = adc_result;
	} else {
		if (adc_result == old_adc_temp_L && !temp_meas_flags.units_changed)
			return;
		old_adc_temp_L = adc_result;
	}
	
	// Check if the temperature measurement exceeds the thermal overload
	// trip temperature. If so, then set the thermal overload flag and
	// disable the relay for the appropriate channel.
	if (adc_result > OVERLOAD_TEMP_ADC) {
		if (channel) {
			channel_status_flags.right_thermal_overload = 1;
			sys_state |= (1 << THML_OVLD_RIGHT_BIT);
			DISABLE_RIGHT_CHANNEL;
		} else {
			channel_status_flags.left_thermal_overload = 1;
			sys_state |= (1 << THML_OVLD_LEFT_BIT);
			DISABLE_LEFT_CHANNEL;
		}
	}
	
	// Check if the temperature measurement is below the thermal overload
	// reset threshold. If so, then, for the appropriate channel, clear the
	// thermal overload flag and, provided there are no other faults or a
	// muted state on the channel, enabnle the channel relay.
	else if (adc_result < RESET_TEMP_ADC) {
		if (channel) {
			channel_status_flags.right_thermal_overload = 0;
			sys_state &= ~((uint8_t)(1 << THML_OVLD_RIGHT_BIT));
			if (!channel_status_flags.right_DC_fault &&
			    !channel_status_flags.right_HF_oscillations &&
				!channel_status_flags.right_muted)
				ENABLE_RIGHT_CHANNEL;
		} else {
			channel_status_flags.left_thermal_overload = 0;
			sys_state &= ~((uint8_t)(1 << THML_OVLD_LEFT_BIT));
			if (!channel_status_flags.left_DC_fault &&
			    !channel_status_flags.left_HF_oscillations &&
				!channel_status_flags.left_muted)
				ENABLE_LEFT_CHANNEL;
		}
	}

	// Check if the temperature measurement is below the range of measurable temperatures.
	// If so, display an indication and set a flag to signal the condition.
	if (adc_result < MIN_TEMP_ADC) {
		LCD_goto(lcd_line,12);
		LCD_putstr_P(PSTR(" LOW TEMP"));
		if (channel)
			temp_meas_flags.temp_limited_R = 1;
		else
			temp_meas_flags.temp_limited_L = 1;
	}
	
	// Check if the temperature measurement is above the range of measurable temperatures.
	// If so, display an indication and set a flag to signal the condition.
	else if (adc_result > MAX_TEMP_ADC) {
		LCD_goto(lcd_line,12);
		LCD_putstr_P(PSTR("HIGH TEMP"));
		if (channel)
			temp_meas_flags.temp_limited_R = 1;
		else
			temp_meas_flags.temp_limited_L = 1;
	}
	
	// The temperature measurement is within the range of measurable temperatures.
	// Look up the temperature display from the ADC measurement and display it on the LCD.
	else {
		// Get the temperature display formatting from the proper look-up table.
		if (temp_meas_flags.units)
			temp_display = pgm_read_word(&adc_to_temp_F_LUT[adc_result]);
		else
			temp_display = pgm_read_word(&adc_to_temp_C_LUT[adc_result]);

		LCD_goto(lcd_line,14);

		hundreds_digit = (char) (temp_display>>12);      // Hundreds digit of temperature
		if (hundreds_digit == 0)
			LCD_putchar(BLANK);
		else
			LCD_putchar(hundreds_digit + '0');

		tens_digit = (char) (temp_display>>8) & 0x0F;    // Tens digit of temperature
		if (hundreds_digit==0 && tens_digit==0)
			LCD_putchar(BLANK);
		else
			LCD_putchar(tens_digit + '0');

		LCD_putchar(((char) temp_display >> 4) + '0');   // Ones digit of temperature
		LCD_putchar('.');                                // Decimal point
		LCD_putchar(((char) temp_display & 0x0F) + '0'); // Tenths digit of temperature
		LCD_putchar(DEGREE);                             // Degrees sign
		LCD_putchar(temp_meas_flags.units ? 'F' : 'C');  // Temperature units

		if (temp_meas_flags.units_changed)
			temp_meas_flags.units_changed--; // Decrement from 2 to 1 if this is the first channel temp update or from 1 to 0 if this is the second channel temp update

		if (channel && temp_meas_flags.temp_limited_R) {
			temp_meas_flags.temp_limited_R = 0;
			LCD_goto(lcd_line,12);
			LCD_putchar(BLANK);
			LCD_putchar(BLANK);
		} else if (!channel && temp_meas_flags.temp_limited_L) {
			temp_meas_flags.temp_limited_L = 0;
			LCD_goto(lcd_line,12);
			LCD_putchar(BLANK);
			LCD_putchar(BLANK);
		}
	}
}


/*
 * INPUT: channel - either 0 or 1. 0 will update the left channel VU meter, while 1 will update
 *                                 the right channel VU meter.
 */
void updateVUMeter(uint8_t channel) {
	uint8_t lcd_line, redraw_tick_marks = 0, adc_lower_bits,
	        adc_upper_bits, new_vu_bars = 0, old_vu_bars, pos;
	uint16_t adc_result;

	// Set the proper ADC input channel to measure the audio VU level. Also determine
	// the line on the LCD to update for the current channel. Finally, store the
	// last # of VU meter bars displayed for the current channel in a local variable.
	if (channel) {
		SET_ADC_INPUT_VU_R;
		lcd_line = 4;

		if (lcd_flags.special_display_right) {
			lcd_flags.special_display_right = 0;
			old_vu_bars = 0;
			redraw_tick_marks = 1;
		}
		else
			old_vu_bars = old_vu_bars_R;
	} else {
		SET_ADC_INPUT_VU_L;
		lcd_line = 2;

		if (lcd_flags.special_display_left) {
			lcd_flags.special_display_left = 0;
			old_vu_bars = 0;
			redraw_tick_marks = 1;
		} else
			old_vu_bars = old_vu_bars_L;
	}

	// Measure the audio VU level from the proper ADC input channel.
	START_AD_CONVERSION;
	WAIT_FOR_ADC;
	adc_lower_bits = ADCL;
	adc_upper_bits = ADCH;
	adc_result = (((uint16_t) adc_upper_bits) << 8) | ((uint16_t) adc_lower_bits);

	// Map the ADC reading to the corresponding number of
	// bars to display for the current VU meter reading.
	while (adc_result > vu_adc_bar_lin_map[new_vu_bars])
		new_vu_bars++;
	
	// If there are fewer bars in the VU meter than before, then overwrite the
	// old bars that are not part of the new bars with meter tick marks.
	if (new_vu_bars < old_vu_bars) {
		LCD_goto(lcd_line, new_vu_bars+1);
		for (pos=new_vu_bars; pos<old_vu_bars; pos++)
			LCD_putchar(vu_meter_tick_marks[pos]);
	}
	
	// If there are more bars in the VU meter than before, display those, starting
	// at the position just after the spot where the old VU bars end. Also, if a
	// special condition (startup, fault or mute) just ended, redraw the rest of
	// the meter tick marks.
	else if (new_vu_bars > old_vu_bars || redraw_tick_marks) {
		LCD_goto(lcd_line, old_vu_bars+1);
		for (pos=old_vu_bars; pos<new_vu_bars; pos++)
			LCD_putchar(BLOCK);
		if (redraw_tick_marks)
			LCD_putstr(&vu_meter_tick_marks[pos]);
	}

	// Store the VU meter value (number of bars displayed) in the appropriate global variable.
	if (channel)
		old_vu_bars_R = new_vu_bars;
	else
		old_vu_bars_L = new_vu_bars;
}

/*
 *
 */
ISR(INT1_vect) {
	RESET_TIMER2;
}


/*
 *
 */
ISR(PCINT0_vect) {
	checkChannelFaultStatus();
}


/*
 *
 */
void checkChannelFaultStatus(void) {
	uint8_t left_fault = 0, right_fault = 0;

	// Check left channel for a DC fault. Set corresponding flag(s) appropriately.
	if (LEFT_DC_FAULT) {
		channel_status_flags.left_DC_fault = 1;
		left_fault = 1;
	} else
		channel_status_flags.left_DC_fault = 0;
	
	// Check left channel for a high frequency oscillations.
	// Set corresponding flag(s) appropriately.
	if (LEFT_HF_OSCILLATIONS) {
		channel_status_flags.left_HF_oscillations = 1;
		left_fault = 1;
	} else
		channel_status_flags.left_HF_oscillations = 0;
	
	// Finally, if there are any faults on the left channel (including thermal
	// overload), or if the left channel is muted, disable (open) the left
	// channel relay. Otherwise, enable (close) it.
	if (left_fault || channel_status_flags.left_thermal_overload || channel_status_flags.left_muted)
		DISABLE_LEFT_CHANNEL;
	else
		ENABLE_LEFT_CHANNEL;


	// Check right channel for a DC fault. Set corresponding flag(s) appropriately.
	if (RIGHT_DC_FAULT) {
		channel_status_flags.right_DC_fault = 1;
		right_fault = 1;
	} else
		channel_status_flags.right_DC_fault = 0;
	
	// Check right channel for a high frequency oscillations.
	// Set corresponding flag(s) appropriately.
	if (RIGHT_HF_OSCILLATIONS) {
		channel_status_flags.right_HF_oscillations = 1;
		right_fault = 1;
	} else
		channel_status_flags.right_HF_oscillations = 0;
	
	// Finally, if there are any faults on the right channel (including thermal
	// overload), or if the right channel is muted, disable (open) the right
	// channel relay. Otherwise, enable (close) it.
	if (right_fault || channel_status_flags.right_thermal_overload || channel_status_flags.right_muted)
		DISABLE_RIGHT_CHANNEL;
	else
		ENABLE_RIGHT_CHANNEL;
}


/*
 *
 */
ISR(TIMER1_COMPA_vect) {
	if (lcd_flags.timer0_schedule_mode) {
		lcd_flags.update_lcd = 1;
		PORTD |= 0x40;
	}
	
	else {
		if (++timer1_tick_count == SS_TIMER0_COUNT)
			ENABLE_SS_BYPASS;
		LCD_putchar(BLOCK);
	}
}


/*
 *
 */
ISR(TIMER2_COMPA_vect) {
	// Disconnect speakers from amplifiers ASAP.
	DISABLE_BOTH_CHANNELS;

	// Prevent any further execution of interrupt service routines.
	DISABLE_INTERRUPTS;

	// 
	if (sys_state != old_sys_state) {
		eeprom_busy_wait();
		eeprom_write_byte((uint8_t *) SYS_STATE_ADDRESS, sys_state);
	}
	
	uint8_t i, x;
	
	// Display shutdown screen.
	LCD_clear();
	LCD_goto(1,1);
	for (i=0; i<20; i++)
		LCD_putchar(BLOCK);
	LCD_goto(2,7);
	LCD_putstr_P(PSTR("Powering"));
	LCD_goto(3,8);
	LCD_putstr_P(PSTR("down..."));
	LCD_goto(4,1);
	for (i=0; i<20; i++)
		LCD_putchar(BLOCK);
	
	// Display animation for shutdown screen.
	for (i=0, x=0xFF; i<10; i++, x^=0xFF) {
		LCD_goto(1,10-i);
		LCD_putchar(x ? SHADE1 : SHADE2);
		LCD_goto(1,11+i);
		LCD_putchar(x ? SHADE2 : SHADE1);
		LCD_goto(4,10-i);
		LCD_putchar(x ? SHADE1 : SHADE2);
		LCD_goto(4,11+i);
		LCD_putchar(x ? SHADE2 : SHADE1);
		_delay_ms(50);
	}

	while (1); // Prevent execution of anything else until complete power down.
}
